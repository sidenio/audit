package utils

import (
	"fmt"
	"reflect"
	"testing"
)

func TestFindFileByRegex(t *testing.T) {
	var tests = []struct {
		i        int
		dir      string
		regex    string
		filesMap map[string]bool
	}{
		{0, "./testdata/", "test", map[string]bool{
			"testdata/testdata0": true,
			"testdata/testdata1": true,
			"testdata/testdata2": true},
		},
		{1, "./testdata/", "data", map[string]bool{
			"testdata/testdata0": true,
			"testdata/testdata1": true,
			"testdata/testdata2": true},
		},
		{2, "./testdata/", "1", map[string]bool{
			"testdata/testdata1": true},
		},
		{3, "./testdata/", "foo", map[string]bool{}},
		{4, "./testdata/", `go\.mod`, map[string]bool{
			"testdata/go.mod": true},
		},
	}
	for i, test := range tests {
		if i != test.i {
			t.Errorf("tests i index miss configured")
		}
		a := FindFileByRegex(test.dir, test.regex)
		t.Log(a)
		if !reflect.DeepEqual(a, test.filesMap) {
			t.Errorf(fmt.Sprintf("TestgetPackageVersionMap i:%d test:%d failed\n", i, test.i))
		} else {
			t.Logf("Passed i:%d ", test.i)
		}
	}
}
