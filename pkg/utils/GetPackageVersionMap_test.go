package utils

import (
	"fmt"
	"reflect"
	"testing"
)

func TestGetPackageVersionMap(t *testing.T) {
	var tests = []struct {
		i                 int
		filename          string
		packageVersionMap map[string]string
	}{
		{0, "./testdata/testdata0", map[string]string{
			"github.com/Shopify/sarama": "v1.32.0"},
		},
		{1, "./testdata/testdata1", map[string]string{
			"github.com/Shopify/sarama": "v1.32.0"},
		},
		{2, "./testdata/testdata2", map[string]string{
			"github.com/Shopify/sarama":   "v1.32.0",
			"github.com/google/uuid":      "v1.3.0",
			"google.golang.org/grpc":      "v1.45.0",
			"google.golang.org/protobuf":  "v1.28.0",
			"go.mongodb.org/mongo-driver": "v1.9.0"},
		},
	}
	for i, test := range tests {
		a := GetPackageVersionMap(test.filename)
		if !reflect.DeepEqual(a, test.packageVersionMap) {
			t.Errorf(fmt.Sprintf("TestgetPackageVersionMap i:%d test:%d failed\n", i, test.i))
		}
	}
}
