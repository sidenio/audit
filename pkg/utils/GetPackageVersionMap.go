package utils

import (
	"bufio"
	"log"
	"os"
	"strings"
)

// GetPackageVersionMap reads an input file, splitting by whitespace,
// Returns a string to string map of ackage name and version
// Lines with leading "//" will be skipped
func GetPackageVersionMap(filename string) (packageVersionMap map[string]string) {

	packageVersionMap = make(map[string]string)

	inputFile, err := os.Open(filename)
	if err != nil {
		log.Fatal("Error opening input file:", err)
	}
	defer inputFile.Close()

	scanner := bufio.NewScanner(inputFile)

	for scanner.Scan() {
		//fmt.Println(scanner.Text())
		line := scanner.Text()
		if strings.HasPrefix(line, "//") {
			continue
		}
		parts := strings.Fields(line)
		packageVersionMap[parts[0]] = parts[1]
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(scanner.Err())
	}

	return packageVersionMap
}
