package utils

import (
	"io/ioutil"
	"log"
	"os"

	"golang.org/x/mod/modfile"
)

// GetPackageVersionsMap reads a go.mod file and returns the required packages
// and versions in a map
// See also: https://pkg.go.dev/golang.org/x/mod/modfile#Parse
func GetPackageVersionsMap(filename string) (modFile *modfile.File, packageVersionsMap map[string]string) {

	packageVersionsMap = make(map[string]string)

	data := readFile(filename)

	var err error

	modFile, err = modfile.Parse(filename, data, nil)
	if err != nil {
		log.Fatal(err)
	}

	// convert list to map
	for _, require := range modFile.Require {
		packageVersionsMap[require.Mod.Path] = require.Mod.Version
	}

	return modFile, packageVersionsMap
}

// readFile helper returning the file as a byte slice
// this just handles errors
func readFile(filename string) (data []byte) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	data, err = ioutil.ReadAll(file)
	if err != nil {
		log.Fatal(err)
	}

	return data
}
