package utils

import (
	"fmt"
)

// PrintSliceSliceString iterates over the map printing keys and values
func PrintSliceSliceString(myMap map[string]string) {
	for key, val := range myMap {
		fmt.Printf("Key: %s, Value: %s\n", key, val)
	}
}
