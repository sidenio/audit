package utils

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
)

// FindFileByRegex does filepath.Walk and regexes each file to find files matching
// Returns filesMap[filename]true
func FindFileByRegex(rootDir string, re string) (filesMap map[string]bool) {

	filesMap = make(map[string]bool)

	regex := regexp.MustCompile(re)

	err := filepath.Walk(rootDir, func(path string, fi os.FileInfo, err error) error {

		if err != nil {
			fmt.Println(err)
			return nil
		}

		if !fi.IsDir() {
			if regex.MatchString(fi.Name()) {
				filesMap[path] = true
			}
		}

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}

	return filesMap
}
