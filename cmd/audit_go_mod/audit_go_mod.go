package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"regexp"

	"gitlab.com/sidenio/audit/pkg/utils"
)

var (
	// Passed by "go build -ldflags" for the show version
	tag    string
	commit string
	date   string
)

func main() {

	version := flag.Bool("version", false, "show version")
	debugLevel := flag.Int("debug", 11, "debug level")
	versionsFile := flag.String("versionsFile", "../../truths/go.mod", "versionsFile path")
	reposDir := flag.String("reposDir", "../../repos", "reposDir is the path with all the git repos")
	flag.Parse()

	if *version {
		fmt.Println("monitor\ttag:", tag, "\tcommit:", commit, "\tcompile date(UTC):", date)
		os.Exit(0)
	}

	goVersionMap := make(map[string]string)
	repoPackageVersionsMap := make(map[string]map[string]string)

	modFile, packageVersionsMap := utils.GetPackageVersionsMap(*versionsFile)
	if *debugLevel > 100 {
		fmt.Printf("modFile.Go.Version: %s\n", modFile.Go.Version)
		utils.PrintSliceSliceString(packageVersionsMap)
	}
	target := "target"
	goVersionMap[target] = modFile.Go.Version
	repoPackageVersionsMap[target] = packageVersionsMap

	repoRegex := regexp.MustCompile(`^../../repos/([^/]+)`)
	for goModFile := range utils.FindFileByRegex(*reposDir, `go\.mod`) {

		// only process go.mod in the root of the repo ( there are other go.mods deeper into the tree )
		if strings.Count(goModFile, "/") != 4 {
			continue
		}
		if *debugLevel > 100 {
			fmt.Printf("goModFile: %s\n", goModFile)
		}

		matches := repoRegex.FindStringSubmatch(goModFile)
		repo := matches[1]

		modFile, packageVersionsMap = utils.GetPackageVersionsMap(goModFile)
		if *debugLevel > 100 {
			fmt.Printf("modFile.Go.Version: %s\n", modFile.Go.Version)
			fmt.Println(goVersionMap)
			fmt.Println(repoPackageVersionsMap)
		}
		goVersionMap[repo] = modFile.Go.Version
		repoPackageVersionsMap[repo] = packageVersionsMap
	}

	//------------------------------------------------
	// Compare go.mod versions

	if *debugLevel > 100 {
		fmt.Println("------------------------------")
		fmt.Println("goVersionMap")
		fmt.Println(goVersionMap)
	}

	for repo, goVersion := range goVersionMap {
		if repo == target {
			continue
		}
		if goVersionMap[target] != goVersion {
			if *debugLevel > 10 {
				fmt.Printf("%s\t%s != %s\n", repo, goVersionMap[target], goVersion)
			}
		}
	}

	//------------------------------------------------
	// Compare package versions

	if *debugLevel > 100 {
		fmt.Println("------------------------------")
		fmt.Println("repoPackageVersionsMap")
		fmt.Println(repoPackageVersionsMap)
	}

	for repo, packageVersionsMap := range repoPackageVersionsMap {
		if repo == target {
			continue
		}
		for targetKey, targetValue := range repoPackageVersionsMap[target] {
			value, isMapContainsKey := packageVersionsMap[targetKey]
			if isMapContainsKey {
				if targetValue != value {
					if *debugLevel > 10 {
						fmt.Printf("%s\t%s\t%s != %s\n", repo, targetKey, targetValue, value)
					}
				}
			}
		}
	}
}
