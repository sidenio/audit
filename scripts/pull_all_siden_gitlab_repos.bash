#!/bin/bash

echo "shellcheck $0"
shellcheck "$0"

if [[ -d "./repos" ]]
then
    echo "./repos exists"
else
    ./scripts/clone_all_siden_gitlab_repos.bash;
    exit 0;
fi

BASE_DIR=$(pwd)
echo "BASE_DIR:$BASE_DIR"
for DIR in ./repos/*
do
    echo "############"
    echo "$DIR: git pull"
    cd "$DIR" || exit
    git pull

    cd "$BASE_DIR" || exit
done
