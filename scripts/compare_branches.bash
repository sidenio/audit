#!/usr/bin/bash

echo "shellcheck $0"
shellcheck "$0"

banch_exists(){
    REGEX=$1
    #echo "REGEX=$REGEX"
    EXISTS=false
    #echo "EXISTS=$EXISTS"
    #git branch -a
    while IFS= read -r -a LINE ; do {
        #echo "LINE:${LINE[0]}"
        if [[ "${LINE[0]}" =~ ${REGEX} ]]; then
            #echo "FOUND!!"
            EXISTS=true
            break

        fi
    };
    done < <(git branch -a | cat); #cat to remove ascii codes (colors)
    #echo "on function end EXISTS=$EXISTS"
}

BASE_DIR=$(pwd)
for DIR in ./repos/*
do
    # echo "############"
    echo "Comparing:$DIR"

    #--------------------
    cd "$DIR" || exit

    #--------------------
    # check for branches
    #echo "############"

    banch_exists "develop"
    DEVELOP_EXISTS=$EXISTS
    #printf 'DEVELOP_EXISTS:%s\n' "$DEVELOP_EXISTS"
    if ! $DEVELOP_EXISTS ; then
        echo "developer branch does not exist!"
        #exit 1
    fi

    banch_exists "master"
    MASTER_EXISTS=$EXISTS
    #printf 'MASTER_EXISTS:%s\n' "$MASTER_EXISTS"

    if ! $MASTER_EXISTS ; then
        banch_exists "main"
        MAIN_EXISTS=$EXISTS
        #printf 'MAIN_EXISTS:%s\n' "$MAIN_EXISTS"
    fi

    #--------------------
    #echo "############-----------------##################"

    if $DEVELOP_EXISTS  ; then

        if $MASTER_EXISTS ; then
            #echo "git --no-pager diff develop...remotes/origin/master"
            #git --no-pager diff develop...remotes/origin/master

            GIT_DIFF=$(git --no-pager diff --stat develop...remotes/origin/master)
            if [ "$(echo "$GIT_DIFF" | wc -l)" -gt 1 ]; then
                echo "############"
                echo "Comparing:$DIR"

                # Rerunning git diff is inefficient, but otherwise we loose the colors :(
                echo "git --no-pager diff --stat develop...remotes/origin/master"
                git --no-pager diff --stat develop...remotes/origin/master
            else
                echo "Comparing:$DIR = nodiff"
            fi
        else
            if $MAIN_EXISTS ; then
                #echo "git --no-pager diff --stat develop...remotes/origin/main"
                GIT_DIFF=$(git --no-pager diff --stat develop...remotes/origin/main)
                if [ "$(echo "$GIT_DIFF" | wc -l)" -gt 1 ]; then
                    echo "############"
                    echo "Comparing:$DIR"

                    echo "git --no-pager diff --stat develop...remotes/origin/main"
                    git --no-pager diff --stat develop...remotes/origin/main
                else
                    echo "Comparing:$DIR = nodiff"
                fi
            else
                echo "############"
                echo "Comparing:$DIR"

                echo "No master or main branches exists!"
            fi
        fi
    fi

    #--------------------
    cd "$BASE_DIR" || exit
done

echo "compare_branches.bash complete"