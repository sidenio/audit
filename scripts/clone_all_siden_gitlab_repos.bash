#!/usr/bin/bash

echo "shellcheck $0"
shellcheck "$0"

repos=$(cat ./truths/siden_repos)
echo "rm -rf repos"
rm -rf repos
echo "mkdir repos"
mkdir repos
cd repos || exit
for repo in $repos
do
    # need all branches to allow comparing the branches (slower though :( )
    #echo "git clone --depth 1 git@gitlab.com:$repo.git"
    #git clone --depth 1 "git@gitlab.com:$repo.git"
    echo "git clone git@gitlab.com:$repo.git"
    git clone "git@gitlab.com:$repo.git"
done
cd .. || exit
