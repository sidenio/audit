all: pull

pull:
	./scripts/pull_all_siden_gitlab_repos.bash

clone:
	./scripts/clone_all_siden_gitlab_repos.bash

clean:
	rm -rf repos
	mkdir repos

compare:
	./scripts/compare_branches.bash
