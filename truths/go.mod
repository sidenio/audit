module target_versions

go 1.17

require (
	github.com/Shopify/sarama v1.33.0
	github.com/google/uuid v1.3.0
	google.golang.org/grpc v1.47.0
	google.golang.org/protobuf v1.28.0
	go.mongodb.org/mongo-driver v1.9.0
)